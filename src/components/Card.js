import React from "react";

const Card = ({name, urlImage, description}) => {
    return (
        <div className="card" >
            <img className="card-img-top img-fluid" src={urlImage} alt="Card image cap"/>
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">{description}</p>
                    <a className="btn btn-primary" onClick={() => console.log(name)}>Comprar</a>
                </div>
        </div>
    )
}

export default Card
