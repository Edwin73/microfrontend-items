import React from "react";
import Header from './Header'

const Container = ({children}) => {
    return (
        <>
            <Header />
            <div>
                {children}
            </div>
        </>
    );
}

export default Container;
