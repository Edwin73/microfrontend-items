import React from 'react';

const Header = () => {
    return (
        <ul className="nav bg-warning justify-content-center">
            <li className="nav-item">
                <Link href="/">
                    <a className="nav-link font-weight-bold">INICIO</a>
                </Link>
            </li>
        </ul>
    );
}

export default Header;
