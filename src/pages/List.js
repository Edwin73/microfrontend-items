import React from "react";
import Card from "../components/Card";

import ITEMS from '../constants/items.json'

const List = () => (
    <div className="container mt-5">
        <div className="row">
            <div className="col-sm-12 text-center">
                <h4>Lista de Productos</h4>
            </div>
            {
                ITEMS.map(
                    (val, index) => <div className="col-sm-4" key={index}><Card {...val} /></div>
                )
            }
        </div>
    </div>
)

export default List
