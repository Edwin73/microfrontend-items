import React from "react";
import {Switch, Route, BrowserRouter as Router} from 'react-router-dom'
import {StylesProvider} from '@material-ui/core/styles'
import 'bootstrap/dist/css/bootstrap.min.css';

import List from './pages/List'

const App = () => (
    <StylesProvider>
        <Router>
            <Switch>
                <Route exact path="/" component={List}/>
            </Switch>
        </Router>
    </StylesProvider>
)

export default App
